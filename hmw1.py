# -*- coding: utf-8 -*-
import sys
import json
import requests

from bs4 import BeautifulSoup

TESTING_CYCLE_LIMITER = 2
TESTING_CATEGORY_LIMITER = 2

class Item(object):
    name = ""
    href = ""
    price = ""
    description = ""    
 
    # The class "constructor" - It's actually an initializer 
    def __init__(self, name, href, price, description):
        self.name = name
        self.href = href
        self.price = price
        self.description = description

def make_item(name, href, price, description):
    item = Item(name, href, price, description)
    return item

def getCategories(seed):
    page=seed
    categories=[]
    try:
        print('Crawled:'+page)
        source = requests.get(page).text
        soup=BeautifulSoup(source, "html5lib")
        data = soup.findAll('ul',attrs={'class':'cat'})
        count = 0
        for div in data:
            count = count + 1
            if count>=TESTING_CATEGORY_LIMITER:
                break  
            links = div.findAll('a')
            for a in links:
                #print "http://www.solar-eshop.cz" + a['href']
                categories.append("http://www.solar-eshop.cz" + a['href'])
    except Exception as e:
            print(e)
    return categories

def getProducts(categories):
    products=[]
    if categories is not None:
        while categories:
            page=categories.pop()
            try:
                print('Crawled:'+page)
                source = requests.get(page).text
                soup=BeautifulSoup(source, "html5lib")
                data=soup.findAll('div',attrs={'class':'item'})
                #one item ~ one product

                count = 0
                for div in data:
                    count = count + 1
                    if count>=TESTING_CYCLE_LIMITER:
                        break                                    
                    priceElem = div.find('strong')
                    price = priceElem.text.strip()
                    #print priceTrimmed.encode(sys.stdout.encoding, errors='replace')     
                    link = div.find('a')
                    href = "http://www.solar-eshop.cz" + link['href']
                    #print "http://www.solar-eshop.cz" + link['href']                    
                    descriptionElem = div.find('p')
                    description = descriptionElem.text.strip()
                    #print description.encode(sys.stdout.encoding, errors='replace')     
                    nameElem = div.find('h2')
                    name = nameElem.text.strip()
                    #print name.encode(sys.stdout.encoding, errors='replace')         
                    
                    product = make_item(name, href, price, description)         

                    #productSerialized=json.dumps(product.__dict__)
                    #print productSerialized
                    products.append(product)

            except Exception as e:
                print(e)
    return products
 
categories=[]
categories = getCategories('http://www.solar-eshop.cz/')
products=[]
products = getProducts(categories)
f1=open('./output.json', 'w')
#f1.write('This is a test')
for product in products:  
    productSerialized=json.dumps(product.__dict__)
    #print productSerialized
    f1.write(productSerialized)
f1.close()